const myapi = getApp().globalData.myapi;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    checks:'',
    leftdata:[],
    rightdata:[]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (e) {
    
    this.setData({
      checks:e.info
    })
    this.getccc();
  },
  getccc(e){
    var _that = this;
    var nickname = getApp().globalData.userInfo.nickName;
    wx.request({
      url: myapi + 'gettype',
      data: { nickname, nickname},
      method: 'POST',
      success: function(res) {
        _that.setData({
          leftdata: res.data.left,
          rightdata: res.data.right
        })
      }
    })
    
  },
  ccccc(e){
    var _that = this;
    var nickname = getApp().globalData.userInfo.nickName;
    var check = e.target.dataset.ttt;
    wx.request({
      url: myapi+'checks',
      data: { nickname: nickname, check: check},
      method: 'post',
      success: function(res) {
          var content = res.data.info
          wx.showModal({
             content: content,
             success: function (res) {
              if (res.confirm) {
                _that.getccc()
              } else {
                _that.getccc()
              }
              }
           })

      },
    })
  }
})

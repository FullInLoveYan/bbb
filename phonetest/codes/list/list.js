const myapi = getApp().globalData.myapi;
Page({
  data:{
    nickname:null,
    users:[],
    leftinfo:[],
    rightinfo:[],
    checks:'',
    ddd:[]
  },
  onLoad(param) {    
    var _that = this;
    var nickname = getApp().globalData.userInfo.nickName;
    wx: wx.request({
      url: myapi + 'getuser',
      data: { nickname: nickname }      
    })
    wx: wx.request({
      url: myapi + 'getnow',
      data: { nickname: nickname },
      success:function(res){
        if (res.data.type == 3){
          wx.navigateTo({
            url: '/codes/checks/checks?info='+res.data.info
          })
        }else{
          _that.setData({
            checks:res.data.info,
            ddd:res.data.data[0]
          })
        }
      }
    })
    console.log(this.data);
  }
})
